import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts:any=[];
  comments:any=[];
  title:string;
  body:string;
  ans:string;
  userId: string;
  postid:string;
  

  constructor(public postsService:PostsService, public authService:AuthService) { }

  
  myFunc(){
    for (let index = 0; index < this.posts.length; index++) {
          this.title = this.posts[index].title;
          this.body = this.posts[index].body;
          this.postsService.addPosts(this.title,this.body);      
    }
    this.ans ="The data retention was successful"
  }
  
  ngOnInit() {
    this.postsService.getPosts().subscribe(posts =>{
      console.log(posts);
      this.posts=posts;
    })

    this.postsService.getComments().subscribe(comments =>{
      console.log(comments);
      this.comments=comments;
    })

    this.authService.user.subscribe(user =>{
      this.userId = user.uid;
    })
    
  }
}
