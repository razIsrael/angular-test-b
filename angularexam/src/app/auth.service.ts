import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private logInErrorSubject = new Subject<string>();
  public err; 
  user: Observable<User | null>

  
  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  getUser(){
    return this.user
  }

  
  signup(email:string, passwoerd:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,passwoerd)
        .then(res =>  {
                      console.log('Succesful sign up',res);
                      this.router.navigate(['/welcome']);
                    })
         .catch (
            error => this.err = error
             )  
  }

  logout(){
    this.afAuth.auth.signOut()
    .then(
      res =>  
       {
         console.log('Succesful logout',res);
         this.router.navigate(['/welcome']);
       }     
   )
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/welcome']);
            }     
        )
        .catch (
          error => this.err = error
           ) 
  }

  constructor(public afAuth:AngularFireAuth,private router:Router) { 
    this.user = this.afAuth.authState; 
  }
}
