//basick
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
//http client &router
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
//my components
import { NavComponent } from './nav/nav.component';
import { AppComponent } from './app.component';
 import { SignupComponent } from './signup/signup.component';
 import { LoginComponent } from './login/login.component';
 import { WelcomeComponent } from './welcome/welcome.component';
import { PostsComponent } from './posts/posts.component';
import { SavedPostComponent } from './saved-post/saved-post.component';

// import { ClassifyComponent } from './classify/classify.component';
// import { CollectionClassComponent } from './collection-class/collection-class.component';
// import { SaveclassifyComponent } from './saveclassify/saveclassify.component';
// import { PostsComponent } from './posts/posts.component';
// import { SavedPostComponent } from './saved-post/saved-post.component';

const appRoutes: Routes = [
   { path: 'singup', component: SignupComponent },
   { path: 'login', component: LoginComponent },
   { path: 'welcome', component: WelcomeComponent },
  // { path: 'dash', component: CollectionClassComponent },
   { path: 'allposts', component: PostsComponent },
   { path: 'savepost', component: SavedPostComponent },
  { path: '',
  redirectTo: '/welcome',
  pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    WelcomeComponent,
    SignupComponent,
    PostsComponent,
    SavedPostComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'test-example'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
