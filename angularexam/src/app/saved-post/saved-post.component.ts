import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostService } from '../post.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-saved-post',
  templateUrl: './saved-post.component.html',
  styleUrls: ['./saved-post.component.css']
})
export class SavedPostComponent implements OnInit {
  
  posts$:Observable<any>;
  userId:string;
  like:number; 

  constructor(private postserv:PostService,public auth:AuthService) { }

  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    this.posts$ = this.postserv.getpostofuser(this.userId);
       }
    )
  }
  addlike(id:string,likes:number){
    this.like = likes + 1 ; 
    this.postserv.updatepostlike(this.userId,id,this.like)
  }

  deletpost(id){
    this.postserv.deletpost(this.userId,id);
   }


}
